package cn.com.bluepanda.antiroot

import android.graphics.Color
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var textView = findViewById<TextView>(R.id.value1)
        if (SecurityUtil.checkDeviceDebuggable()) {
            textView.text = getString(R.string.ui_unpass)
            textView.setTextColor(Color.rgb(255,  0, 0))
        } else {
            textView.text = getString(R.string.ui_pass)
            textView.setTextColor(Color.rgb(0,  255, 0))
        }

        textView = findViewById(R.id.value2)
        if (SecurityUtil.checkSuperuserApk()) {
            textView.text = getString(R.string.ui_unpass)
            textView.setTextColor(Color.rgb(255,  0, 0))
        } else {
            textView.text = getString(R.string.ui_pass)
            textView.setTextColor(Color.rgb(0,  255, 0))
        }

        textView = findViewById(R.id.value3)
        if (SecurityUtil.checkRootPathSU()) {
            textView.text = getString(R.string.ui_unpass)
            textView.setTextColor(Color.rgb(255,  0, 0))
        } else {
            textView.text = getString(R.string.ui_pass)
            textView.setTextColor(Color.rgb(0,  255, 0))
        }

        textView = findViewById(R.id.value4);
        if (SecurityUtil.checkBusyBox()) {
            textView.text = getString(R.string.ui_unpass)
            textView.setTextColor(Color.rgb(255,  0, 0))
        } else {
            textView.text = getString(R.string.ui_pass)
            textView.setTextColor(Color.rgb(0,  255, 0))
        }

        textView = findViewById(R.id.value5);
        if (SecurityUtil.checkAccessRootData()) {
            textView.text = getString(R.string.ui_unpass)
            textView.setTextColor(Color.rgb(255,  0, 0))
        } else {
            textView.text = getString(R.string.ui_pass)
            textView.setTextColor(Color.rgb(0,  255, 0))
        }

        textView = findViewById(R.id.value6);
        if (SecurityUtil.checkGetRootAuth()) {
            textView.text = getString(R.string.ui_unpass)
            textView.setTextColor(Color.rgb(255,  0, 0))
        } else {
            textView.text = getString(R.string.ui_pass)
            textView.setTextColor(Color.rgb(0,  255, 0))
        }
    }
}
