package cn.com.bluepanda.antiroot

import android.os.Build
import android.util.Base64
import android.util.Log
import java.io.*
import java.security.KeyFactory
import java.security.MessageDigest
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher

/**
 * <pre>
 * 【v1.4更新记录(2016-03-28) -- By 王锦阳】
 * 1. 新增RSA加密算法
 * 【v1.3更新记录(2012-11-02) -- By 王锦阳】
 * 1. 改进需要加密的字符串的读取编码规则，由之前版本中的固定值改为现在可自定义修改编码方式的；
 * 2. 新增还原默认编码格式的方法；
 * 【v1.2更新记录(2012-10-12) -- By 王锦阳】
 * 1. 新增 EncryType 枚举，替代之前版本中使用的字符串方式，最大限度减少因外部给错参数而导致的加密失败；
 * 【v1.1更新记录(2012-09-28) -- By 王锦阳】
 * 1. 更新加密方式，支持 MD2、MD5、SHA-1、SHA-256、SHA-384、SHA-512 算法；
 * 2. 去除测试用的 main() 方法；
 * 【v1.0更新记录(2011-04-16) -- By 王锦阳】
 * 1. 原始版本创建，主要用于 MD5 加密功能；
</pre> *
 * 程序安全加密工具类：用于对字符串进行MD5和SHA-1加密的引擎类
 * @author 王锦阳
 * @version 1.3
 * @since 2012-11-02
 */
object SecurityUtil {
    /** 加密字符串字符集格式的常量字符串  */
    private var newCharset = Encode.UTF_8

    /**
     * 加密方法：实现字符串加密的方法
     * @param encryptType 加密类型（请使用本类提供的常量字符串）
     * @param contentString 传入的待加密字符串
     * @return 返回加密字符串
     */
    fun encryption(
        encryptType: EncryptType,
        contentString: String,
        vararg publicKeys: String?
    ): String? {
        if (encryptType == EncryptType.RSA) {
            if (publicKeys.isEmpty()) {
                throw RuntimeException("public key is null.")
            }
            return execRSA(encryptType, contentString, publicKeys[0])
        }
        return execMessageDigest(encryptType, contentString)
    }

    private fun execMessageDigest(
        encryptType: EncryptType,
        contentString: String
    ): String? { // 捕捉加密过程中产生的异常
        try { // 创建加密引擎类对象
            val md =
                MessageDigest.getInstance(encryptType.value)
            // 传入需要加密的字符串
            md.update(contentString.toByteArray(charset(newCharset.value)))
            // 执行加密并保存到字节数组中
            val b = md.digest()
            var result: Int
            // 声明StringBuffer对象
            val buffer = StringBuilder()
            // 将引擎类处理过的加密数据进行转换
            for (aB in b) {
                result = aB.toInt()
                if (result < 0) result += 256
                if (result < 16) buffer.append("0")
                // 得出计算结果并存入StringBuffer对象
                buffer.append(Integer.toHexString(result))
            }
            // 返回加密后的结果
            return buffer.toString()
        } catch (e: Exception) { // 如果出错则打印提示并记录
            e.printStackTrace()
        }
        return null
    }

    private fun execRSA(
        encryptType: EncryptType,
        contentString: String,
        publicKeyStr: String?
    ): String? {
        var result: String? = null
        val algorithmName = "RSA/ECB/PKCS1Padding"
        try {
            val buffer =
                Base64.decode(publicKeyStr, Base64.DEFAULT)
            val keyFactory =
                KeyFactory.getInstance(encryptType.value)
            val keySpec =
                X509EncodedKeySpec(buffer)
            val publicKey = keyFactory.generatePublic(keySpec)
            val cipher = Cipher.getInstance(algorithmName)
            cipher.init(Cipher.ENCRYPT_MODE, publicKey)
            val encryptByte = cipher.doFinal(contentString.toByteArray())
            result = Base64.encodeToString(encryptByte, Base64.NO_WRAP)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return result
    }

    fun restoreDefaultCharset() {
        newCharset = Encode.UTF_8
    }

    fun setNewCharset(newCharset: Encode?) {
        if (newCharset != null) {
            SecurityUtil.newCharset = newCharset
        }
    }

    //check buildTags
    //Superuser.apk
    //find su in some path
    //if (checkRootWhichSU()){return true;}//find su use 'which'
    //find su use 'which'
    //find su use 'which'
    //exec su
    val isDeviceRooted: Boolean
        get() { //check buildTags
            if (checkDeviceDebuggable()) {
                return true
            }
            //Superuser.apk
            if (checkSuperuserApk()) {
                return true
            }
            //find su in some path
            if (checkRootPathSU()) {
                return true
            }
            //if (checkRootWhichSU()){return true;}//find su use 'which'
            if (checkBusyBox()) {
                return true
            } //find su use 'which'
            return if (checkAccessRootData()) {
                true
            } else checkGetRootAuth() //find su use 'which'
            //exec su
        }

    fun checkDeviceDebuggable(): Boolean {
        val buildTags = Build.TAGS
        return buildTags != null && buildTags.contains("test-keys")
    }

    fun checkSuperuserApk(): Boolean {
        try {
            val file = File("/system/app/Superuser.apk")
            if (file.exists()) {
                return true
            }
        } catch (ignored: Exception) {
        }
        return false
    }

    fun checkRootPathSU(): Boolean {
        var f: File
        val kSuSearchPaths = arrayOf(
            "/system/bin/",
            "/system/xbin/",
            "/system/sbin/",
            "/sbin/",
            "/vendor/bin/"
        )
        try {
            for (kSuSearchPath in kSuSearchPaths) {
                f = File(kSuSearchPath + "su")
                if (f.exists()) {
                    return true
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }

    @Synchronized
    fun checkBusyBox(): Boolean {
        return try {
            val strCmd = arrayOf("busybox", "df")
            val execResult =
                executeCommand(strCmd)
            execResult != null
        } catch (e: Exception) {
            false
        }
    }

    @Synchronized
    fun checkGetRootAuth(): Boolean {
        var process: Process? = null
        var os: DataOutputStream? = null
        return try {
            process = Runtime.getRuntime().exec("su")
            os = DataOutputStream(process.outputStream)
            os.writeBytes("exit\n")
            os.flush()
            val exitValue = process.waitFor()
            exitValue == 0
        } catch (e: Exception) {
            false
        } finally {
            try {
                os?.close()
                process?.destroy()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    fun executeCommand(shellCmd: Array<String>?): List<String>? {
        var line: String
        val fullResponse: MutableList<String> =
            ArrayList()
        val localProcess: Process
        localProcess = try {
            Runtime.getRuntime().exec(shellCmd)
        } catch (e: Exception) {
            return null
        }
        val `in` =
            BufferedReader(InputStreamReader(localProcess.inputStream))
        try {
            while (`in`.readLine().also { line = it } != null) {
                fullResponse.add(line)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return fullResponse
    }

    @Synchronized
    fun checkAccessRootData(): Boolean {
        return try {
            val fileContent = "test_ok"
            val writeFlag = writeFile(fileContent)
            Log.d("BP_LOG", "check root data = $writeFlag")
            val strRead = readFile()
            fileContent == strRead
        } catch (e: Exception) {
            false
        }
    }

    //写文件
    private fun writeFile(message: String): Boolean {
        return try {
            val fout = FileOutputStream("/data/su_test")
            val bytes = message.toByteArray()
            fout.write(bytes)
            fout.close()
            true
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    //读文件
    private fun readFile(): String? {
        val file = File("/data/su_test")
        return try {
            val fis = FileInputStream(file)
            val bytes = ByteArray(1024)
            val bos = ByteArrayOutputStream()
            var len: Int
            while (fis.read(bytes).also { len = it } > 0) {
                bos.write(bytes, 0, len)
            }
            String(bos.toByteArray())
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    /**
     * 加密类型枚举内部类
     * @author 王锦阳
     * @version 1.0
     */
    enum class EncryptType(val key: Int, val value: String) {
        RSA(0, "RSA"), MD2(1, "MD2"), MD5(2, "MD5"), SHA_1(3, "SHA-1"), SHA_256(
            4,
            "SHA-256"
        ),
        SHA_384(5, "SHA-384"), SHA_512(6, "SHA-512");

    }
}